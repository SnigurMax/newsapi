﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IDataManager dataManager;

        public ValuesController(IDataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        [HttpGet]
        public ActionResult<IEnumerable<News>> Get()
        {
            try
            {
                var res = dataManager.GetAllNews();

                if (res != null)
                    return Ok(res);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return BadRequest();
            }

            return Ok();
        }
    }
}
