﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using WebApi.Models;

namespace WebApi.Services
{
    public class DataManager : IDataManager
    {
        public IEnumerable<News> GetAllNews()
        {
            using (StreamReader sr = new StreamReader(Path.Combine((string)AppDomain.CurrentDomain.GetData("ContentRootPath"), "news.json")))
            {
                var json = JsonConvert.DeserializeObject<List<News>>(sr.ReadToEnd());
                return json;
            }
        }
    }
}
